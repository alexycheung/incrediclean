require File.expand_path('../boot', __FILE__)

require 'csv'
require 'rails/all'
require 'twilio-ruby'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Incrediclean
  class Application < Rails::Application
    #set time zone
    config.time_zone = 'Pacific Time (US & Canada)'

    #API key for image hosting with filepicker
    config.filepicker_rails.api_key = "A4FBPdkwRQ0SiZSnMdIPTz"
    config.action_view.embed_authenticity_token_in_remote_forms = true

    #preconfigure Twiio client
    Twilio.configure do |config|
		  config.account_sid = 'ACca6bbce29336225c553d42eb9ad16df5'
		  config.auth_token = '2ac2722f8fa54f71315e881840fd81e8'
		end
  end
end

#set number of records displayed per page
WillPaginate.per_page=25
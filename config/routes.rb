Rails.application.routes.draw do
	authenticated :user do
		root to: 'users#show', as: :authenticated_root
	end

	devise_for :users, controllers: { registrations: "users/registrations" } 
	devise_scope :user do
	  get "signin", to: "devise/sessions#new", as: "signin"
	  get "free-trial", to: "users/registrations#new", as: "free_trial"
	end
	resources :users do
		member do
			get 	'tag'
			get 	'rack'
			get 	'remember_rack'
			get		'forget_rack'
			patch 'confirm_password'
			get 	'lost_receipt'
			get 	'read_intro'
			get 	'read_tutorial'
			patch 'set_rack'
		end
	end
	match '/:id/urgent', to: 'users#urgent', via: 'get', as: 'urgent'
	match '/:id/products', to: 'users#products', via: 'get', as: 'user_products'
	match '/:id/employees', to: 'users#employees', via: 'get', as: 'user_employees'
	match '/:id/analytics', to: 'users#analytics', via: 'get', as: 'user_analytics'

	root to: 'pages#home'
	match '/terms', to: 'pages#terms', via: 'get', as: 'terms'

	resources :customers do
		collection { post :import }
		member do
			patch 'update_name'
			patch 'update_phone'
			patch 'update_text'
			patch 'update_starch'
			get 	'place_order'
			get		'details'
			get		'order_history'
			get 	'checkout'
			get		'pay_with_cash'
			get		'productless_item'
			get		'charge_later'
			get 	'pay_with_card'
		end
	end

	resources :products do
		member do
			get 'load_edit'
		end
	end

	resources :categories

	resources :items do
		member do
			patch 'update_price'
		end
	end

	resources :orders do
		member do
			get 	'details'
			get 	'pick_up'
			get 	'search_rack'
			get 	'search_tags'
			get 	'add_order_number'
			get 	'print_receipt'
			patch 'rack'
			get 	'confirm_delete'
			patch 'delete_order'
			get		'pay_on_pickup'
			patch 'leave_note'
			get		'load_urgent_rack'
			patch 'rack_urgent_order'
			get		'pickup_pay_with_cash'
			get		'pickup_pay_with_card'
		end
	end

	resources :cards do
		member do
			get 'pay'
			get 'pickup_pay'
		end
	end

	resources :employees

	resources :checkins do
		member do
			get 	'show_more'
			get 	'change_start_time'
			get 	'change_finish_time'
			patch 'update_start_time'
			patch 'update_finish_time'
		end
	end

	resources :tutorials do
		member do
			get 'open'
		end

		collection do
			post 'sort'
		end
	end
	match '/tutorial', to: 'tutorials#user_tutorial', via: 'get', as: 'user_tutorial'

	resources :messages
end
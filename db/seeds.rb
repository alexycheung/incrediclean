#create first user
User.create!(name: "Alex Cheung", email: "alex@incrediclean.me", phone: "4084899038", password: "Manonthemoon1", password_confirmation: "Manonthemoon1", admin: true, stripe_customer_token: "none", address: "330 Crescent Village Circle", city: "San Jose")

#create categories
category_names = ['Alterations', 'Household Goods', 'Clothes']

category_names.each do |category_name|
	Category.create(name: category_name)
end


#create products divided by categories
clothes = [
	[ 'Laundered Shirt' , 2, "https://www.filepicker.io/api/file/okZ0Jk42Rg2jhJ8TzTXH" ],
	[ 'Drycleaned Shirt' , 5, "https://www.filepicker.io/api/file/IKMVddscRjyeZcdDC7P8" ],
	[ 'Pants' , 5, "https://www.filepicker.io/api/file/GyXyeqaCRYyUHDVKWzWG" ],
	[ 'Sweater' , 5, "https://www.filepicker.io/api/file/EXyB0zFcQz6qjOfkx2y4" ],
	[ 'Jacket' , 7, "https://www.filepicker.io/api/file/3VI4Z4q3RGavSZ35V8eg" ],
	[ 'Dress' , 8, "https://www.filepicker.io/api/file/kZkEvg2PSEqXDQEMxsUt" ],
	[ 'Skirt' , 5, "https://www.filepicker.io/api/file/XkSiE6fASynUX3WXoLXW" ],
	[ '3/4 Coat' , 12, "https://www.filepicker.io/api/file/Q47qJTtyTnk8rWzij2Sw" ],
	[ 'Overcoat' , 15, "https://www.filepicker.io/api/file/9pydrwJFTQ20b3qgY67w" ],
	[ 'Heavy Jacket' , 12, "https://www.filepicker.io/api/file/PxOV2jzvTrGWsw6oy7vA" ],
	[ 'Scarf' , 5, "https://www.filepicker.io/api/file/Iiyt9M1QPeUwNjNRUfZ4" ],
	[ 'Tie' , 5, "https://www.filepicker.io/api/file/nNkGE85TSSCu438Fe0XC" ],
	[ 'Bag' , 15, "https://www.filepicker.io/api/file/gwNh3W3TRue9SPturZ1G" ],
	[ 'Wedding Dress' , 350, "https://www.filepicker.io/api/file/4ktD2cvTU6yGotjFVJmS" ],
	[ 'Glove' , 8, "https://www.filepicker.io/api/file/VTEnCOV9QoeAYMCtpZKV" ],
	[ 'Hat' , 8, "https://www.filepicker.io/api/file/ECsqM95iSsyXNmUD51Mi" ]
]


household_goods = [
	[ 'Tablecloth' , 10, "https://www.filepicker.io/api/file/XnAt6XqRdadsERR8YLot" ],
	[ 'Pillow' , 10, "https://www.filepicker.io/api/file/dWAoKPbRqyJZuDdOt2TX" ],
	[ 'Pillowcase' , 5, "https://www.filepicker.io/api/file/F5u5SxiRESYECdKQdYzX" ],
	[ 'Curtain' , 15 ],
	[ 'Cushion' , 10 ],
	[ 'Blanket' , 25, "https://www.filepicker.io/api/file/Sg2DR5czS8qszGfvXy1c" ],
	[ 'Valance' , 18, "https://www.filepicker.io/api/file/3v0CQOLS2u4A8iYu0mBw" ],
	[ 'Table Runner' , 8 ],
	[ 'Drapery' , 3, "https://www.filepicker.io/api/file/71j6PYK7QVWxCJvM0HPK" ],
	[ 'Napkin' , 5, "https://www.filepicker.io/api/file/5ua2wBb0TW6cDiVpdl7M" ],
	[ 'Down Comfort' , 45, "https://www.filepicker.io/api/file/OedgchP2TI2EV3z05qYr" ],
	[ 'Bedspread' , 25, "https://www.filepicker.io/api/file/Zht8tk5ORtWEmGRVMvj2" ],
	[ 'Rug' , 18, "https://www.filepicker.io/api/file/9g0gIQfGQYu7TasHc8Z0" ]
]


alterations = [
	[ 'Hemming' , 14 ],
	[ 'Replace Button' , 1 ],
	[ 'Mend Hole' , 5 ],
	[ 'Replace Hook' , 4],
	[ 'Taper Dress' , 24 ],
	[ 'Taper Skirt' , 16 ],
	[ 'Taper Pants' , 16 ],
	[ 'Take In Waist' , 16 ],
	[ 'Shorten Sleeve' , 24 ]
]

clothes.each do |product|
	Product.create!(name: product[0], price: product[1], category_id: 3, sold:0, user_id:1, cleaning_method: "dryclean", img:product[2])
end

household_goods.each do |product|
	Product.create!(name: product[0], price: product[1], category_id: 2, sold:0, user_id:1, cleaning_method: "dryclean", img: product[2])
end

alterations.each do |product|
	Product.create!(name: product[0], price: product[1], category_id: 1, sold:0, user_id:1, cleaning_method: "other", img: product[2])
end
class RemovePaidFieldFromCheckins < ActiveRecord::Migration
  def change
  	remove_column :checkins, :paid, :boolean
  end
end

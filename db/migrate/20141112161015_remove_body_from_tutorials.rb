class RemoveBodyFromTutorials < ActiveRecord::Migration
  def change
  	remove_column :tutorials, :body, :text
  end
end

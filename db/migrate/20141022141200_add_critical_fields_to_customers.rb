class AddCriticalFieldsToCustomers < ActiveRecord::Migration
  def change
  	add_column :customers, :text, :boolean, default: false
  	add_column :customers, :starch, :string, default: "none"
  end
end
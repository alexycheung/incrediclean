class AddReadTutorialToUsers < ActiveRecord::Migration
  def change
    add_column :users, :read_tutorial, :boolean, default: false, null: false
  end
end

class AddStripeCustomerTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :stripe_customer_token, :string, default: "token"
    change_column :users, :stripe_customer_token, :string, default: "token", null: false
    change_column_default(:users, :stripe_customer_token, nil)
  end
end
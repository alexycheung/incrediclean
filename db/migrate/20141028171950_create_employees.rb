class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
    	t.string :name, null: false
    	t.integer :phone, null: false
    	t.integer :user_id, null: false
      t.timestamps
    end

    change_column :employees, :phone, :bigint, null: false
    add_index :employees, :user_id
  end
end

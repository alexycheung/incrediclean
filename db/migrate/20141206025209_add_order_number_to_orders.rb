class AddOrderNumberToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :order_number, :integer, default: 1, null: false
  end
end

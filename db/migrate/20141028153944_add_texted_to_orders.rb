class AddTextedToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :texted, :boolean, default: false
    change_column :orders, :texted, :boolean, default: false, null: false
  end
end

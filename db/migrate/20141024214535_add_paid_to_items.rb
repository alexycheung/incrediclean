class AddPaidToItems < ActiveRecord::Migration
  def change
    add_column :items, :paid, :boolean, default: false, null: false
    add_index :items, :paid
  end
end

class AddUserIdToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :user_id, :integer
    change_column :customers, :user_id, :integer, null:  false
    add_index :customers, :user_id
  end
end
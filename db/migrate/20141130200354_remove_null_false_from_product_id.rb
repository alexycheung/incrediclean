class RemoveNullFalseFromProductId < ActiveRecord::Migration
  def change
  	change_column :items, :product_id, :integer, null: true
  end
end

class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
    	t.decimal :total, null: false
    	t.integer :user_id, null: false
    	t.integer :customer_id, null: false
    	t.integer :card_id
      t.timestamps
    end

    add_index :orders, [:user_id, :customer_id]
  end
end

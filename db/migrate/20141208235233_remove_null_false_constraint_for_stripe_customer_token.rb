class RemoveNullFalseConstraintForStripeCustomerToken < ActiveRecord::Migration
  def change
  	change_column :users, :stripe_customer_token, :string, null: true
  end
end

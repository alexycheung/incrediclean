class AddPhoneToUsers < ActiveRecord::Migration
  def change
    add_column :users, :phone, :string, default: "incrediclean"
    change_column :users, :phone, :string, default: "incrediclean", null: false
    change_column_default(:users, :phone, nil)
  end
end

class AddOrderNumberIndexToOrders < ActiveRecord::Migration
  def change
  	remove_index :orders, :id
  	add_index :orders, :order_number
  end
end

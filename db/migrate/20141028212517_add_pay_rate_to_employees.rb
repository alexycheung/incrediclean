class AddPayRateToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :pay_rate, :decimal, default: 0
    change_column :employees, :pay_rate, :decimal, default: 0, null: false
    change_column_default(:employees, :pay_rate, nil)
  end
end

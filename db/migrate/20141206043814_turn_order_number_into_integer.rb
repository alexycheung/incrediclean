class TurnOrderNumberIntoInteger < ActiveRecord::Migration
  def change
  	change_column :orders, :order_number, :string, default: "1", null: false
  end
end

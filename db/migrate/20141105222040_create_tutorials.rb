class CreateTutorials < ActiveRecord::Migration
  def change
    create_table :tutorials do |t|
    	t.string :title, null: false
    	t.string :video
    	t.text :transcript
      t.timestamps
    end
  end
end

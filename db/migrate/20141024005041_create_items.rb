class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
    	t.integer :product_id, null: false
    	t.integer :order_id
    	t.decimal :price, precision: 8, scale: 2, null: false
    	t.integer :quantity, default: 1, null: false
    	t.string :name, null: false
    	t.integer :customer_id, null: false

      t.timestamps
    end

    add_index :items, [:order_id]
  end
end
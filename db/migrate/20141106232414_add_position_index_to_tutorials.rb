class AddPositionIndexToTutorials < ActiveRecord::Migration
  def change
  	add_index :tutorials, :position
  end
end

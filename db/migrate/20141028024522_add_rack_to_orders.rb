class AddRackToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :rack, :string
  end
end

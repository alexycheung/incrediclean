class AddCleaningMethodToProducts < ActiveRecord::Migration
  def change
    add_column :products, :cleaning_method, :string, default: "laundered"
    change_column :products, :cleaning_method, :string, default: "laundered", null: false
    change_column_default(:products, :cleaning_method, nil)
  end
end
class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
    	t.string :name, null: false
    	t.integer :phone, null: false
    	t.string :address
    	t.string :city
    	t.string :state

      t.timestamps
    end

    change_column :customers, :phone, :bigint, null: false
  end
end
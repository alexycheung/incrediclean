class AddIndexToCheckins < ActiveRecord::Migration
  def change
  	add_index :checkins, [:employee_id, :paid]
  end
end

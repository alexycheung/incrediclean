class AddSoldToProducts < ActiveRecord::Migration
  def change
    add_column :products, :sold, :integer, default: 0
    change_column :products, :sold, :integer, default: 0, null: false
  end
end
class AddReadIntroToUsers < ActiveRecord::Migration
  def change
    add_column :users, :read_intro, :boolean, default: false, null: false
  end
end

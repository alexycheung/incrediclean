class RemoveReadIntroAndReadTutorialFromUsers < ActiveRecord::Migration
  def change
  	remove_column :users, :read_intro, :boolean
  	remove_column :users, :read_tutorial, :boolean
  end
end

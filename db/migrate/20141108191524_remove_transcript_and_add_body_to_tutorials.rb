class RemoveTranscriptAndAddBodyToTutorials < ActiveRecord::Migration
  def change
  	add_column :tutorials, :body, :text
  	remove_column :tutorials, :transcript, :text
  end
end

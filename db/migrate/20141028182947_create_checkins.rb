class CreateCheckins < ActiveRecord::Migration
  def change
    create_table :checkins do |t|
    	t.integer :employee_id, null: false
    	t.boolean :paid, default: false, null: false
    	t.datetime :finish
    	t.decimal :total
      t.timestamps
    end
  end
end

class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
    	# precision is the total amount of digits
			# scale is the number of digits right of the decimal point
    	t.decimal :price, precision: 8, scale: 2, null: false
    	t.string :name, null: false
    	t.string :img

      t.timestamps
    end
  end
end

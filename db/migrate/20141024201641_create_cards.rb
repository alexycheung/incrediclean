class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
    	t.integer :last4, null: false
    	t.string :brand, null: false
    	t.string :stripe_customer_token, null: false
    	t.string :stripe_card_token
    	t.integer :customer_id, null: false

      t.timestamps
    end

    add_index :cards, :customer_id
  end
end
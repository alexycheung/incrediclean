class AddPickupToCard < ActiveRecord::Migration
  def change
  	add_column :cards, :pickup, :boolean, default: false
  end
end

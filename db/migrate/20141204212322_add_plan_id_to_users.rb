class AddPlanIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :plan_id, :string, default: "1", null: false
  end
end

class RemovePhoneIndexFromCustomers < ActiveRecord::Migration
  def change
  	remove_index :customers, :phone
  end
end

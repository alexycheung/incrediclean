class AddPickedUpToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :picked_up, :boolean, default: false
    change_column :orders, :picked_up, :boolean, default: false, null: false
    add_index :orders, :picked_up
  end
end

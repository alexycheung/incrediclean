class AddCriticalIndexesToCustomers < ActiveRecord::Migration
  def change
  	add_index :customers, :name
  	add_index :customers, :phone
  end
end

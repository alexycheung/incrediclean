class ChangePhoneToStringForCustomers < ActiveRecord::Migration
  def change
  	change_column :customers, :phone, :string, null: false
  end
end

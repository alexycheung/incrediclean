class AddTurnaroundTimeToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :turnaround_time, :integer, default: 3, null: false
  end
end
class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
    	t.string :name, null: false

      t.timestamps
    end

    add_column :products, :category_id, :integer, default: 1
    change_column :products, :category_id, :integer, default: 1, null: false
    add_index :products, :category_id
  end
end
class RemoveDefaultForTurnaroundTime < ActiveRecord::Migration
  def change
  	change_column_default(:users, :turnaround_time, nil)
  end
end

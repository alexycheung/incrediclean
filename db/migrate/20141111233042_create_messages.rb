class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
    	t.string :emotion
    	t.string :message_type
    	t.text :content, null: false
    	t.integer :user_id, null: false
      t.timestamps
    end

    add_index :messages, :user_id
  end
end
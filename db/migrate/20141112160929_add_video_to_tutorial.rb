class AddVideoToTutorial < ActiveRecord::Migration
  def change
  	add_column :tutorials, :video, :string, null: false
  end
end

class AddPaymentTypeToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :payment_type, :string, default: "card", null: false
  end
end
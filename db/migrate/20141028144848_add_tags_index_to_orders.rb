class AddTagsIndexToOrders < ActiveRecord::Migration
  def change
  	add_index :orders, :tags
  end
end

class AddCleaningMethodIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, :cleaning_method
  end
end
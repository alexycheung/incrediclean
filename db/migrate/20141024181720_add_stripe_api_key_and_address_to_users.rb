class AddStripeApiKeyAndAddressToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :stripe_secret_key, :string
  	add_column :users, :stripe_publishable_key, :string
  	add_column :users, :address, :string
  	add_column :users, :city, :string
  end
end

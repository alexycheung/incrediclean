# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141209000114) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cards", force: true do |t|
    t.integer  "last4",                                 null: false
    t.string   "brand",                                 null: false
    t.string   "stripe_customer_token",                 null: false
    t.string   "stripe_card_token"
    t.integer  "customer_id",                           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "pickup",                default: false
  end

  add_index "cards", ["customer_id"], name: "index_cards_on_customer_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "checkins", force: true do |t|
    t.integer  "employee_id", null: false
    t.datetime "finish"
    t.decimal  "total"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customers", force: true do |t|
    t.string   "name",                        null: false
    t.string   "phone",                       null: false
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id",                     null: false
    t.boolean  "text",       default: false
    t.string   "starch",     default: "none"
  end

  add_index "customers", ["name"], name: "index_customers_on_name", using: :btree
  add_index "customers", ["user_id"], name: "index_customers_on_user_id", using: :btree

  create_table "employees", force: true do |t|
    t.string   "name",                 null: false
    t.integer  "phone",      limit: 8, null: false
    t.integer  "user_id",              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "pay_rate",             null: false
  end

  add_index "employees", ["user_id"], name: "index_employees_on_user_id", using: :btree

  create_table "items", force: true do |t|
    t.integer  "product_id"
    t.integer  "order_id"
    t.decimal  "price",       precision: 8, scale: 2,                 null: false
    t.integer  "quantity",                            default: 1,     null: false
    t.string   "name",                                                null: false
    t.integer  "customer_id",                                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "paid",                                default: false, null: false
    t.integer  "category_id"
  end

  add_index "items", ["order_id"], name: "index_items_on_order_id", using: :btree
  add_index "items", ["paid"], name: "index_items_on_paid", using: :btree

  create_table "messages", force: true do |t|
    t.string   "emotion"
    t.string   "message_type"
    t.text     "content",      null: false
    t.integer  "user_id",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "orders", force: true do |t|
    t.decimal  "total",                         null: false
    t.integer  "user_id",                       null: false
    t.integer  "customer_id",                   null: false
    t.integer  "card_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "picked_up",    default: false,  null: false
    t.string   "rack"
    t.string   "tags"
    t.boolean  "texted",       default: false,  null: false
    t.datetime "pickup_time"
    t.boolean  "paid",         default: false
    t.text     "note"
    t.string   "payment_type", default: "card", null: false
    t.string   "order_number", default: "1",    null: false
  end

  add_index "orders", ["order_number"], name: "index_orders_on_order_number", using: :btree
  add_index "orders", ["picked_up"], name: "index_orders_on_picked_up", using: :btree
  add_index "orders", ["tags"], name: "index_orders_on_tags", using: :btree
  add_index "orders", ["user_id", "customer_id"], name: "index_orders_on_user_id_and_customer_id", using: :btree

  create_table "products", force: true do |t|
    t.decimal  "price",           precision: 8, scale: 2,             null: false
    t.string   "name",                                                null: false
    t.string   "img"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sold",                                    default: 0, null: false
    t.integer  "user_id",                                             null: false
    t.integer  "category_id",                             default: 1, null: false
    t.string   "cleaning_method",                                     null: false
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id", using: :btree
  add_index "products", ["cleaning_method"], name: "index_products_on_cleaning_method", using: :btree
  add_index "products", ["user_id"], name: "index_products_on_user_id", using: :btree

  create_table "queue_classic_jobs", force: true do |t|
    t.text     "q_name",                       null: false
    t.text     "method",                       null: false
    t.json     "args",                         null: false
    t.datetime "locked_at"
    t.integer  "locked_by"
    t.datetime "created_at", default: "now()"
  end

  add_index "queue_classic_jobs", ["q_name", "id"], name: "idx_qc_on_name_only_unlocked", where: "(locked_at IS NULL)", using: :btree

  create_table "tutorials", force: true do |t|
    t.string   "title",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position"
    t.string   "video",      null: false
  end

  add_index "tutorials", ["position"], name: "index_tutorials_on_position", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.boolean  "admin",                  default: false, null: false
    t.string   "stripe_secret_key"
    t.string   "stripe_publishable_key"
    t.string   "address"
    t.string   "city"
    t.string   "phone",                                  null: false
    t.string   "stripe_card_token"
    t.string   "stripe_customer_token"
    t.integer  "turnaround_time",                        null: false
    t.string   "plan_id",                default: "1",   null: false
  end

  add_index "users", ["admin"], name: "index_users_on_admin", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

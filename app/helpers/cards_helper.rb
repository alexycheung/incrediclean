module CardsHelper
	#if customer already has card, hide new card form
	def any_cards(cards)
		"hide hidden-card-form" if cards.any?
	end
end

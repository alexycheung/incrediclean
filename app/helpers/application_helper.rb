module ApplicationHelper
	#style header
	def header_background
		"transparent-header" if current_page?(root_path)
	end

	#style body
	def body_background
		"grey-background" if user_signed_in?
	end

	#style width of alert
	def alert_width
		"signed-in-alert" if user_signed_in?
	end

	#Returns full title on a per-page basis.
	def full_title(page_title)
		base_title = "Incrediclean"
		if page_title.empty?
			base_title
		else
			"#{base_title} | #{page_title}"
		end
	end

	def footer_css
		"grey-footer" if signed_in?
	end

	def highlight(path)
		"selected" if current_page?(path)
	end

	#change width of yield-container based on whether user is signed in or not
	def full_width_container
		"full-width" if !user_signed_in?
	end
end

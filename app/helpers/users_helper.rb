module UsersHelper
	#determine search form route based on whether current user is an admin
	def customer_search_url(user)
		if current_user.admin
			user_path(user)
		else
			root_path
		end
	end
end

module OrdersHelper
	def display_pick_up_order(order)
		"hide" if current_page?(root_path)
	end

	#color _pick_up_order differently if it has not been paid yet
	def paid_or_not(order)
		"unpaid-order" if order.items.not_paid.any?
	end
end

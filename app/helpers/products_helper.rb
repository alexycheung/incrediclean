module ProductsHelper
	#used in products/_edit_product
	def show_image(product)
		if product.img == nil || product.img == ""
			"Add Image"
		else
			"#{filepicker_image_tag product.img, fit: 'crop', w: 94, h:94}"
		end
	end
end
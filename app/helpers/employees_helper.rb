module EmployeesHelper
	#assign correct root path based on whether user is logged in yet
	def assign_root_path
		if signed_in?
			user_path(current_user)
		else
			root_path
		end
	end
end

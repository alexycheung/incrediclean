class MessageNotifier < ActionMailer::Base
  default from: "hello@incrediclean.me"

  #when user submits message, send email
  def send_message_email(message)
  	@message = message
  	@user = @message.user
  	mail(
  		to: "hello@incrediclean.me",
  		subject: "#{@user.name}: #{@message.emotion if !@message.emotion.blank?} : #{@message.message_type if !@message.message_type.blank?}"
  	)
  end
end

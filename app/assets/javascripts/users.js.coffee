ready = ->
  #user chooses monthly or yearly plan
  $(".monthly-plan").on "click", ->
    $("#user_plan_id").val("1")
    $(".monthly-plan").addClass("selected")
    $(".yearly-plan").removeClass("selected")
    return

  $(".yearly-plan").on "click", ->
    $("#user_plan_id").val("2")
    $(".yearly-plan").addClass("selected")
    $(".monthly-plan").removeClass("selected")
    return

  $(document).on "contextmenu", (e) ->
    e.preventDefault()
    return

  #display form to change item price on click
  $("#yield-container").on "mousedown", ".item-box", (e) ->
    switch e.which
      when 3
        $id = $(this).attr('id').split('-')[2]
        $update_price = $("#update-price-" + $id)
        $update_price.parent("#modal-overlay").show ->
          $update_price.children(".item_price").children("#item_price").select()
          return
    return

  #focus on price when clicking productless-item-box
  $("#yield-container").on "click", ".productless-item-box", (e) ->
    $(this).children(".edit_item").children().children("#item_quantity").select()
    return

  #run stripe javascript
  $(document).stripe_subscription() if $("#new_user")[0]

  #run highcharts javascript
  highCharts() if $("#revenue-chart")[0]


  #search after pause
  $(".search-field").on "keyup", ->
    value = $(".search-field").val()
    setTimeout (->
      $(".search-customers").submit() if $(".search-field").val() is value
      return
    ), 200
    return

  #load form to add productless item
  $("#yield-container").on "click", ".add-productless-item", (e) ->
    $('.productless-item-overlay').show()
    $('.productless-item-form #item_category_id').val $(this).attr("id").split("-")[1]
    $('.productless-item-form #item_name').focus()
    return

  #save order note form
  $("#yield-container").on "keyup", "#order_note", (e) ->
    $this = $(this)
    $value = $(this).val()
    setTimeout (->
      $this.parent().parent("form").submit() if $this.val() is $value
      return
    ), 200
    return


  #load datepicker for analytics date search
  $('[data-behaviour~=datepicker]').datepicker(autoclose:true) if $('[data-behaviour~=datepicker]')[0]

  #automatically open up order if user searched for it
  $(".order").hide() if $("#order-list")[0]

  #submit item quantity form whenever there's a change
  $("#yield-container").on "keyup", "#item_quantity", (e) ->
    $(this).parent().parent(".edit_item").submit() if $(".edit_item")[0]
    return

  #select item quantity field on click
  $("#yield-container").on "click", "#item_quantity", ->
    $(this).select()
    return

  #submit name or phone form when focusing out
  $("#yield-container").on "blur", "#customer_name", ->
    $(this).parent().parent(".edit_customer").submit() if $(".edit_customer")[0]
    return

  $("#yield-container").on "blur", "#customer_phone", ->
    $(this).parent().parent(".edit_customer").submit() if $(".edit_customer")[0]
    return

  #show new card form on click; hide new card on back click
  $("body").on "click", ".use-new-card", ->
    $(".or-separator, .pay-with-cash, #card-list, .use-new-card").hide()
    $(".charge-later").hide() if $(".charge-later")[0]
    $('.hidden-card-form').removeClass("hide")
    $('#card_number').focus()
    $('.modal-footer').append("<span class='more-options'>Back To Other Options</span>") if !$('.more-options')[0]
    return

  $("body").on "click", ".more-options", ->
    $(this).parent(".modal-footer").html("")
    $(".hidden-card-form").addClass("hide")
    $(".charge-later").show() if $(".charge-later")[0]
    $(".or-separator, .pay-with-cash, #card-list, .use-new-card").show()
    return


  #hide sections for place order, order history, and details for customers on click

  $("#yield-container").on "click", ".close-place-order", ->
    $(this).parent("#show-customer-container").children(".place-order-link").show()
    $(this).next("#checkout-category-list").remove()
    $(".toolbar").remove() if $(".toolbar")[0]
    $(this).remove()
    $("html, body").animate
      scrollTop: $(".exit-customer-container").offset().top
    , 150
    return

  $("#yield-container").on "click", ".close-details", ->
    $(this).parent("#show-customer-container").children(".details-link").show()
    $(this).next("#customer-details").remove()
    $(this).remove()
    $("html, body").animate
      scrollTop: $(".exit-customer-container").offset().top
    , 150
    return

  $("#yield-container").on "click", ".close-order-history", ->
    $(this).parent("#show-customer-container").children(".order-history-link").show()
    $("#order-history").remove()
    $(this).remove()
    $("html, body").animate
      scrollTop: $(".exit-customer-container").offset().top
    , 150
    return

  #hide customers/_customer on exit button click
  $("#yield-container").on "click", ".exit-customer-container", ->
    $(this).parent('#show-customer-container').prev('.customer').show()
    $(this).parent('#show-customer-container').remove()
    $(".toolbar").remove() if $(".toolbar")[0]
    return

  #hide order details on exit button click
  $("body").on "click", ".exit-order-container", ->
    $(this).parent('.order-details').prev('.order').show()
    $(this).parent('.order-details').remove()
    return

  #click button to show new customer form

  $("body").on "click", ".add-customer-button", ->
    $("#new_customer").parent("#modal-overlay").show()
    $("#customer_name").focus()
    return

  #click button to show new employee form
  $("body").on "click", ".add-employee-button", ->
    $("#modal-overlay").fadeIn()
    $("#employee_name").focus()
    return

  #click button to show new product form

  $("#index-products-container").on "click", ".add-product-button", ->
    $("#modal-overlay").fadeIn()
    $("#product_category_id").val $(this).parent(".product-list").attr("id").split("-")[1]
    $("#product_name").focus()
    return

	#click button to exit customer form

  $("body").on "click", ".exit-form", ->
    $(this).parent().parent("#modal-overlay").fadeOut ->
      $(".rack-order").parent("#modal-overlay").remove() if $(".rack-order")[0]
      $(".tag-order").parent("#modal-overlay").remove() if $(".tag-order")[0]
      $(".edit_employee").parent("#modal-overlay").remove() if $(".edit_employee")[0]
      $("#new_message").parent("#modal-overlay").remove() if $("#new_message")[0]
      $(".edit-product-overlay").remove() if $(".edit-product-overlay")[0]
      $("#new_card").parent("#modal-overlay").remove() if $("#new_card")[0]
      $(".rack-urgent-order").parent("#modal-overlay").remove() if $(".rack-urgent-order")[0]
      return
    return

$.fn.stripe_subscription = ->
  $(document).ready ->
    jQuery ->
      Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'))
      cc.setupForm()
    cc =
      setupForm: ->
        $('#new_user').submit ->
          $('button[type=submit]').attr('disabled', true)
          if $('#card_number').length
            cc.processCard()
            false
          else
            true
      processCard: ->
        $("#new_user button").html "<i class='fa fa-spinner fa-spin'></i>"
        card =
          number: $('#card_number').val()
          expMonth: $('#card_month').val()
          expYear: $('#card_year').val()
        Stripe.createToken(card, cc.handleStripeResponse)
      handleStripeResponse: (status, response) ->
        if status == 200
          $('#user_stripe_card_token').val(response.id)
          $('#new_user')[0].submit()
        else
          $('#error_explanation').remove()
          $('#stripe_error').fadeIn().text(response.error.message)
          $("#new_user button").html "Start Your Trial"
          $('button[type=submit]').attr('disabled', false)
          $("html, body").animate
            scrollTop: $("#stripe_error").offset().top
          , 150
  return

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)
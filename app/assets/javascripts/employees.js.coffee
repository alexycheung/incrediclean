ready = ->
  #hide customers/_customer on exit button click
  $("#employee-list").on "click", ".exit-timesheet", ->
    $('#timesheet').prev('.employee').show()
    $('#timesheet').remove()
    return

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)
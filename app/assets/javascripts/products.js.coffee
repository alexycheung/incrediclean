ready = ->
  #show image after uploading from filepicker
  window.onImageUpload = ->
    file = event.fpfile
    if $('.add-product-image')[0]
      img = $("<img>").prop("src", file.url)
      img.attr "src", img.attr("src") + "/convert?w=94&h=94&fit=crop"
      $(".add-product-image").html(img)
    else if $('.add-tutorial-video:visible')[0]
      $('.add-tutorial-video').html('<video></video>')
      video = $("<video>").prop("src", file.url)
      $('.add-tutorial-video').html(video)
    else if $('.tutorial-details')[0]
      $(".tutorial-details video").attr("src", file.url)


	#click button to exit product form

  $("#index-products-container").on "click", ".exit-product-form", ->
    $("#modal-overlay").fadeOut ->
      $(".edit-product-overlay").remove() if $(".edit-product-overlay")[0]
      return

  #shitty short-term fix to filepicker not loading with turbolinks
  setTimeout (->
    unless $(".add-product-image")[0] || $(".add-tutorial-video")[0]
      $("input[type=filepicker]").each ->
        filepicker.constructWidget this
        return

    return
  ), 100

#make coffeescript work with turbolinks redirect

$(document).ready(ready)
$(document).on('page:load', ready)
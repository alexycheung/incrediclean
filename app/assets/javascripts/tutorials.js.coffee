ready = ->
	#click button to show new tutorial form
	$("#tutorials-container").on "click", ".add-tutorial-button", ->
    $("#modal-overlay").fadeIn()
    $("#tutorial_title").focus()
    return

  #to reposition tutorails with drag and drop
  $('#tutorial-list').sortable
    axis: 'y'
    update: ->
      $.post($(this).data('update-url'), $(this).sortable('serialize'))

  #close opened tutorial on click
  $('#tutorial-list').on "click", ".exit-tutorial-container", ->
    $(this).parent('.tutorial-details').prev('.tutorial').show();
    $(this).parent('.tutorial-details').remove();
    return

  #close opened user tutorial on click
  $('#user-tutorial-container').on "click", ".exit-tutorial-container", ->
    $(this).parent().prev('.user-tutorial').show()
    $(this).parent().remove();
    return

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)
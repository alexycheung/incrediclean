ready = ->
  $(document).stripe_js() if $("#new_card")[0]

$.fn.stripe_js = ->
  jQuery ->
    cc.setupForm()
  cc =
    setupForm: ->
      $('#new_card').submit ->
        $('button[type=submit]').attr('disabled', true)
        if $('#card_number').length
          cc.processCard()
          false
        else
          true
    processCard: ->
      $("#new_card button").html "<i class='fa fa-spinner fa-spin'></i>"
      card =
        number: $('#card_number').val()
        expMonth: $('#card_month').val()
        expYear: $('#card_year').val()
      Stripe.createToken(card, cc.handleStripeResponse)
    handleStripeResponse: (status, response) ->
      if status == 200
        $('#card_stripe_card_token').val(response.id)
        $.rails.handleRemote($('#new_card'))
      else
        $('#card_number, #card_month, #card_year').val('')
        $('#card_number').focus()
        $('#stripe_error').fadeIn().text(response.error.message)
        $("#new_card button").html "Charge Card"
        $('button[type=submit]').attr('disabled', false)

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)
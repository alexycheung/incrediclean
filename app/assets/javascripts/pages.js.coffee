ready = ->
	#scroll down on click for home page
	$(".learn-more").click ->
	  $("html, body").animate
	    scrollTop: $("#feature-list-wrap").offset().top
	  , 400
	  return

	#resize images
	$(document).resize_image()

#resize image
$.fn.resize_image = ->
	$(document).ready ->
		if $(".feature")[0]
			$(".feature img").css height: $(".feature img").width()/1.6
		return
	return

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)
$(window).resize(ready)
ready = ->
	$("#header a").hover (-> # Mouse over
		$("#header a").not(this).animate opacity: "0.4", 300
		return
	), -> # Mouse out
		$("#header a").not(this).animate opacity: "1", 300
		return

	#activate tooltip
	$(".sidebar-link").tooltip()

	#highlight icons on navbar on click immediately
	$("body").on "click", "#sidebar .sidebar-link", (e) ->
		$('.selected').removeClass "selected"
		$(this).addClass "selected"
		$("#yield-container").html "<div class='page-loading'><i class='fa fa-spin fa-circle-o-notch'></i></div>"
		return

	#scroll back up to header on click
	$(".back-to-top").click ->
	  $("html, body").animate
	    scrollTop: $("body").offset().top
	  , 150, ->
	  	$('.search-field').focus() if $('.search-field')[0]
	  	return
	  return

	#only show button to scroll up if it's below 700px
	$(window).scroll ->
	  if $(window).scrollTop() >= 700
	    $(".back-to-top").fadeIn()
	  else
	    $(".back-to-top").fadeOut()
	  return

	#remove alerts after 10 seconds
	$(".alert").delay(5000).slideUp()

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)
class Item < ActiveRecord::Base
	#validations
	validates :customer_id, presence: true, numericality: true
	validates :price, presence: true, numericality: true
	validates :quantity, presence: true, numericality: true
	validates :name, presence: true

	#relationships
	belongs_to :order
	belongs_to :product
	belongs_to :customer
	belongs_to :category

	#scopes
	scope :not_paid, -> {where(paid: false)}
	scope :with_order, -> {where("order_id IS NOT NULL")}
	scope :orderless, -> {where("order_id IS NULL")}
	scope :paid, -> {where(paid: true)}
end
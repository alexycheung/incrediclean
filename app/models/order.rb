#generate bar code
require 'barby'
require 'barby/barcode/code_128'

class Order < ActiveRecord::Base
	searchkick

	#auto-increment order number
	before_create :set_order_number

	#validations
	validates :total, presence: true, numericality: true
	validates :user_id, presence: true, numericality: true
	validates :customer_id, presence: true, numericality: true
	validates :payment_type, presence: true
	validates :order_number, presence: true, numericality: true

	#relationships
	belongs_to :user
	belongs_to :customer
	belongs_to :card
	has_many :items, dependent: :destroy

	#scopes
	scope :recently, -> {order("created_at DESC")}
	scope :recently_updated, -> {order("updated_at DESC")}
	scope :not_picked_up, -> {where(picked_up:false)}
	scope :tagless, -> {where("tags IS NULL")}
	scope :not_paid, -> {where(paid:false)}
	scope :urgent, -> {not_picked_up.where("rack IS NULL").where("pickup_time <= ?", DateTime.now)}

	#to generate barcode
	include HasBarcode

	has_barcode :barcode,
    outputter: :svg,
    type: :code_128,
    code128: 'B',
    value: Proc.new { |p| "#{p.number}" }

	def number
		self.order_number
	end

	def pickup_pay_with_card
		order = self

		#mark items and order as paid and add to order
		order.update_attributes(paid:true, picked_up:true, pickup_time:DateTime.now, payment_type: "card")

		#items that belong to order but are not paid yet
		unpaid_items = order.items

		unpaid_items.update_all(paid:true)

		#change products' sold field
		unpaid_items.each do |item|
			if item.product_id
				product = item.product
				product.sold += item.quantity
				product.save
			end
		end
	end

	def pickup_pay_with_cash
		order = self

		#mark items and order as paid and add to order
		order.update_attributes(paid:true, picked_up:true, pickup_time:DateTime.now, payment_type: "cash")

		#items that belong to order but are not paid yet
		unpaid_items = order.items

		unpaid_items.update_all(paid:true)

		#change products' sold field
		unpaid_items.each do |item|
			if item.product_id
				product = item.product
				product.sold += item.quantity
				product.save
			end
		end
	end

	#text customer
	def self.deliver(id)
		order = Order.find(id)

		Twilio::REST::Client.new.messages.create(
		  from: '+14086805317',
		  to: order.customer.phone,
		  body: "Hi #{order.customer.name}! Your order ##{order.order_number} is ready for pick up! - #{order.user.name}"
		)
	end

  private
  	#auto-increment order number
  	def set_order_number
  		user = self.user
  		orders = user.orders

  		unless !orders.any?
  			self.order_number = (orders.recently.first.order_number.to_i+1).to_s
  		end
  	end
end

class Message < ActiveRecord::Base
	#validations
	validates :content, presence: true
	validates :user_id, presence: true

	#relationships
	belongs_to :user

	#scopes
	scope :recently, -> {order("created_at DESC")}
end
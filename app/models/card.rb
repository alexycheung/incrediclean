class Card < ActiveRecord::Base
	#validations
	validates :last4, presence: true
	validates :brand, presence: true
	validates :stripe_customer_token, presence: true
	validates :customer_id, presence: true

	#relationships
	belongs_to :customer
	has_many :orders

	#scopes
	scope :recently, -> {order("created_at DESC")}

	def charge_new_card_on_pickup(order)
		customer = self.customer

		#create customer
  	stripe_customer = Stripe::Customer.create(
		  description: customer.name,
		  card: self.stripe_card_token
		)

		#save stripe customer token to card
    self.stripe_customer_token = stripe_customer.id
    self.pickup = false

    #save values from charge
	  stripe_card = stripe_customer.cards.retrieve(Stripe::Customer.retrieve(self.stripe_customer_token).default_card)
	  self.brand = stripe_card.brand
	  self.last4 = stripe_card.last4

    save!

    self.charge_card_on_pickup(order)
	rescue Stripe::InvalidRequestError => e
	  logger.error "Stripe error while creating customer: #{e.message}"
	  errors.add :base, "There was a problem with your credit card."
	  false
	end

	def charge_card_on_pickup(order)
		card = self

		total = order.total

		#create charge
	  charge = Stripe::Charge.create(
		  amount: (total*100.0).to_i,
		  currency: "usd",
		  customer: card.stripe_customer_token,
		  description: "Charged #{customer.name} $#{total}"
		)

		#mark items and order as paid and add to order
		order.update_attributes(paid:true, picked_up:true, pickup_time:DateTime.now, card_id: self.id)

		#items that belong to order but are not paid yet
		unpaid_items = order.items

		unpaid_items.update_all(paid:true)

		#change products' sold field
		unpaid_items.each do |item|
			if item.product_id
				product = item.product
				product.sold += item.quantity
				product.save
			end
		end
	rescue Stripe::InvalidRequestError => e
	  logger.error "Stripe error while creating customer: #{e.message}"
	  errors.add :base, "There was a problem with your credit card."
	  false
	end

	def charge_card
		customer = self.customer
		user = customer.user

		#calculate order total
		unpaid_items = customer.items.not_paid.orderless
    total = 0

    unpaid_items.each do |item|
    	total += (item.quantity * item.price)
    end

		#create charge
	  charge = Stripe::Charge.create(
		  amount: (total*100.0).to_i,
		  currency: "usd",
		  customer: self.stripe_customer_token,
		  description: "Charged #{customer.name} $#{total}"
		)

	  #create order
		order = Order.create(total: total, customer_id: customer.id, user_id: user.id, card_id: self.id, paid: true, pickup_time: 3.business_days.after(DateTime.now))
		order.save

		#mark items as paid and add to order
		unpaid_items.update_all(paid:true, order_id:order.id, created_at: order.created_at)

		#change products' sold field
		unpaid_items.each do |item|
			if item.product_id
				product = item.product
				product.sold += item.quantity
				product.save
			end
		end
	rescue Stripe::InvalidRequestError => e
	  logger.error "Stripe error while creating customer: #{e.message}"
	  errors.add :base, "There was a problem with your credit card."
	  false
	end

	def charge_new_card
		customer = self.customer

		#create customer
  	stripe_customer = Stripe::Customer.create(
		  description: customer.name,
		  card: self.stripe_card_token
		)

		#save stripe customer token to card
    self.stripe_customer_token = stripe_customer.id

    #save values from charge
	  stripe_card = stripe_customer.cards.retrieve(Stripe::Customer.retrieve(self.stripe_customer_token).default_card)
	  self.brand = stripe_card.brand
	  self.last4 = stripe_card.last4

    save!

    self.charge_card
	rescue Stripe::InvalidRequestError => e
	  logger.error "Stripe error while creating customer: #{e.message}"
	  errors.add :base, "There was a problem with your credit card."
	  false
	end
end
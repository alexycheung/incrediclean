class Employee < ActiveRecord::Base
	#validations
	validates :name, presence: true
	validates :phone, presence: true, numericality: true
	validates :user_id, presence: true, numericality: true
	validates :pay_rate, presence: true, numericality: true

	#relationships
	belongs_to :user
	has_many :checkins

	#scopes
	scope :recently, -> {order("created_at DESC")}
end

class Tutorial < ActiveRecord::Base
	#automatically increment position
	acts_as_list
	
	#validations
	validates :title, presence: true
	validates :video, presence: true

	#scopes
	scope :by_position, -> {order("position")}
end

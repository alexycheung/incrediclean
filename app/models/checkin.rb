class Checkin < ActiveRecord::Base
	#validations
	validates :employee_id, presence: true

	#relationships
	belongs_to :employee

	#scopes
	scope :recently, -> {order("created_at DESC")}
end

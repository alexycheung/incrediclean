class Customer < ActiveRecord::Base
	#search functionality
	searchkick word_start: [:name]

	#validations
	validates :name, presence: true
	validates :phone, presence: true
	validates :user_id, presence: true

	#relationships
	belongs_to :user
	has_many :items
	has_many :orders
	has_many :cards

	#scopes
	scope :recently_updated, -> {order("updated_at DESC")}

	#import customers by csv; format => (name, phone)
	def self.import(file, user)
		user_id = user.id

	  SmarterCSV.process(file.path, col_sep: ',') do |row|
	  	row.each {|a| a[:user_id]=user_id}
	  	Customer.create! row
	  end  
	end

	#calculate total before checking out
	def calculate_total
		total = 0.0

		self.items.not_paid.orderless.each do |item|
			total += (item.quantity * item.price)
		end

		return total
	end

	#charge customer later; don't mark items as paid yet
	def charge_customer_later
		customer = self
		user = customer.user

		#calcualte total
		total = 0

		unpaid_items = customer.items.not_paid.orderless

		unpaid_items.each do |item|
			total += (item.quantity * item.price)
		end

		#create order
		order = Order.create(total: total, customer_id: customer.id, user_id: user.id, paid: false, pickup_time: 3.business_days.after(DateTime.now))
		order.save

		unpaid_items.update_all(paid:false, order_id:order.id, created_at: order.created_at)
	end

	#pay with card (if user doesn't have stripe keys saved under acccount)
	def pay_with_card
		customer = self
		user = customer.user

		#calculate total
		total = 0

		unpaid_items = customer.items.not_paid.orderless

		unpaid_items.each do |item|
    	total += (item.quantity * item.price)
    end

		#create order
		order = Order.create(total: total, customer_id: customer.id, user_id: user.id, paid: true, pickup_time: 3.business_days.after(DateTime.now))
		order.save

		#mark items as paid and add to order
		unpaid_items.update_all(paid:true, order_id:order.id, created_at: order.created_at)

		#change products' sold field
		unpaid_items.each do |item|
			if item.product_id
				product = item.product
				product.sold += item.quantity
				product.save
			end
		end
	end

	#pay with cash
	def pay_with_cash
		customer = self
		user = customer.user

		#calculate total
		total = 0

		unpaid_items = customer.items.not_paid.orderless

		unpaid_items.each do |item|
    	total += (item.quantity * item.price)
    end

		#create order
		order = Order.create(total: total, customer_id: customer.id, user_id: user.id, paid: true, pickup_time: 3.business_days.after(DateTime.now), payment_type: "cash")
		order.save

		#mark items as paid and add to order
		unpaid_items.update_all(paid:true, order_id:order.id, created_at: order.created_at)

		#change products' sold field
		unpaid_items.each do |item|
			if item.product_id
				product = item.product
				product.sold += item.quantity
				product.save
			end
		end
	end
end
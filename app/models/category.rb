class Category < ActiveRecord::Base
	#relationships
	has_many :products
	has_many :items

	#scopes
	scope :recently, -> {order("created_at DESC")}
end
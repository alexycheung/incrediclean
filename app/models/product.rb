class Product < ActiveRecord::Base
	#validations
	validates :name, presence: true
	validates :price, presence: true, numericality: true
	validates :sold, presence: true, numericality: true
	validates :user_id, presence: true
	validates :cleaning_method, presence: true

	#relationships
	belongs_to :user
	belongs_to :category
	has_many :items

	#scopes
	scope :recently, -> {order("created_at DESC")}
	scope :by_sales, -> {order("sold DESC")}
end

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #validations
  validates :name, presence: true
  validates :phone, presence: true
  validates :address, presence: true
  validates :turnaround_time, presence: true, numericality: true

  #relationships
  has_many :customers
  has_many :products
  has_many :orders
  has_many :employees
  has_many :subscriptions
  has_many :messages

  #scopes
  scope :recently, -> {order("created_at DESC")}

  #create $50/month subscription + 7-day free trial
  def create_subscription
    customer = Stripe::Customer.create(
      :card => self.stripe_card_token,
      :plan => self.plan_id,
      :email => self.email
    )

    self.stripe_customer_token = customer.id
  end

  #calculate total order revenue by day
  def total_volume_by_day(day, start_date)
    orders.where("created_at >= ? AND created_at <=?",(start_date+day.days).beginning_of_day, (start_date+day.days).end_of_day).sum(:total)
  end

  #calculate total order revenue by week
  def total_volume_by_week(week, start_date)
    orders.where("created_at >= ? AND created_at <=?",start_date+week.weeks, (start_date+week.weeks+1.week-1.day).end_of_day).sum(:total)
  end

  #calculate total order revenue by month
  def total_volume_by_month(month, start_date)
    orders.where("created_at >=? AND created_at <=?", start_date+month.months, (start_date+month.months+1.month).end_of_day).sum(:total)
  end

  #calculate total order revenue by year
  def total_volume_by_year(year, start_date)
    orders.where("created_at >=? AND created_at <=?", start_date+year.years, (start_date+year.years+1.year).end_of_day).sum(:total)
  end

  #calculate total workload divided by categories
  def total_workload_by_day(day, start_date, method)
    products.where(cleaning_method: method).joins(:items).where("items.created_at >= ? AND items.created_at <=?", (start_date+day.days).beginning_of_day, (start_date+day.days).end_of_day).sum(:quantity)
  end

  #calculate total workload divided by categories / week
  def total_workload_by_week(week, start_date, method)
    products.where(cleaning_method: method).joins(:items).where("items.created_at >= ? AND items.created_at <=?", start_date+week.weeks, (start_date+week.weeks+1.week-1.day).end_of_day).sum(:quantity)
  end

  #calculate total workload divided by categories / month
  def total_workload_by_month(month, start_date, method)
    products.where(cleaning_method: method).joins(:items).where("items.created_at >= ? AND items.created_at <=?", start_date+month.months, (start_date+month.months+1.month).end_of_day).sum(:quantity)
  end
end

class CheckinsController < ApplicationController
	def update_start_time
		@checkin = Checkin.find(params[:id])
		@updated = @checkin.update_attributes(checkin_params)
		respond_to do |format|
			format.js
		end
	end

	def update_finish_time
		@checkin = Checkin.find(params[:id])
		@updated = @checkin.update_attributes(checkin_params)
		respond_to do |format|
			format.js
		end
	end

	def change_start_time
		@checkin = Checkin.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def change_finish_time
		@checkin = Checkin.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def show_more
		@checkin = Checkin.find(params[:id])
		@employee = @checkin.employee
		@index = @employee.checkins.recently.index(@checkin)
		@checkins = @employee.checkins.recently.take(@index+11)
		@height_difference = @employee.checkins.recently.index(@checkins.last)-@index
		respond_to do |format|
			format.js
		end
	end

	def create
		@checkin = Checkin.new(checkin_params)
		@employee = @checkin.employee
		@saved = @checkin.save
		@checkins = @employee.checkins.recently.take(10)
		respond_to do |format|
			format.js
		end
	end

	def update
		@checkin = Checkin.find(params[:id])
		@employee = @checkin.employee
		#record finish time
		@checkin.finish = DateTime.now
		#calculate checkin total
		@checkin.total = TimeDifference.between(@checkin.created_at.to_time, @checkin.finish.to_time).in_hours * @employee.pay_rate
		@updated = @checkin.update_attributes(checkin_params)
		@checkins = @employee.checkins.recently.take(10)
		respond_to do |format|
			format.js
		end
	end

	private
		def checkin_params
			params.require(:checkin).permit(:employee_id, :paid, :finish, :total)
		end
end

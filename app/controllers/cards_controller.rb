class CardsController < ApplicationController
	def destroy
		@card = Card.find(params[:id])
		@destroyed = @card.destroy
		respond_to do |format|
			format.js
		end
	end

	def create
		@card = Card.new(card_params)
		@customer = @card.customer
		@user = @customer.user
		
		Stripe.api_key = @card.customer.user.stripe_secret_key

		unless params[:order_id]
			@card.charge_new_card
			@order = @card.orders.recently.first
			@saved_on_dropoff = true
		else
			@order = Order.find(params[:order_id])
			@card.charge_new_card_on_pickup(@order)
			@saved_on_pickup = true
		end

		@products = @customer.user.products
		@categories = Category.recently

		respond_to do |format|
			format.js
		end
	rescue Stripe::CardError => e
		@error = true
		respond_to do |format|
			format.js
		end
	end

	def pay
		@card = Card.find(params[:id])
		Stripe.api_key = @card.customer.user.stripe_secret_key
		@saved = @card.charge_card
		@order = @card.orders.recently.first
		respond_to do |format|
			format.js
		end
	rescue Stripe::CardError => e
		@error = true
		respond_to do |format|
			format.js
		end
	end

	def pickup_pay
		@card = Card.find(params[:id])
		@order = Order.find(params[:order_id])
		Stripe.api_key = @card.customer.user.stripe_secret_key
		@saved = @card.charge_card_on_pickup(@order)
		respond_to do |format|
			format.js
		end
	rescue Stripe::CardError => e
		@error = true
		respond_to do |format|
			format.js
		end
	end

	private
		def card_params
			params.require(:card).permit(:pickup, :order_id, :last4, :brand, :stripe_customer_token, :stripe_card_token, :customer_id)
		end
end

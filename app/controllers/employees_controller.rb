class EmployeesController < ApplicationController
	def create
		@employee = Employee.new(employee_params)
		@saved = @employee.save
		respond_to do |format|
			format.js
		end
	end

	def show
		@employee = Employee.find(params[:id])
		@checkins = @employee.checkins.recently.take(10)
		respond_to do |format|
			format.js
		end
	end

	def edit
		@employee = Employee.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def update
		@employee = Employee.find(params[:id])
		@checkins = @employee.checkins.recently.take(10)
		@updated = @employee.update_attributes(employee_params)
		respond_to do |format|
			format.js
		end
	end

	private
		def employee_params
			params.require(:employee).permit(:name, :phone, :user_id, :pay_rate)
		end
end

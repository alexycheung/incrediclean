class ItemsController < ApplicationController
	def update_price
		@item = Item.find(params[:id])
		@updated = @item.update_attributes(update_item_params)
		@customer = @item.customer
		@total = @customer.calculate_total
		respond_to do |format|
			format.js
		end
	end

	def update
		@item = Item.find(params[:id])
		@customer = @item.customer
		@updated = @item.update_attributes(update_item_params)
		@total = @customer.calculate_total
		respond_to do |format|
			format.js
		end
	end

	def create
		@item = Item.new(item_params)
		@customer = @item.customer

		#only save item if it doesn't exist yet

		if @item.product_id
			@product = @item.product

			unless Item.orderless.where(product_id: @product.id, customer_id: @item.customer_id, paid: false).present?
				@item.name = @product.name
				@item.price = @product.price
			end
		end

		if @item.product_id || @item.category_id
			@saved = @item.save
			@total = @customer.calculate_total
		end

		respond_to do |format|
			format.js
		end
	end

	def destroy
		@item = Item.find(params[:id])
		@product = @item.product
		@customer = @item.customer
		@destroyed = @item.destroy
		@total = @customer.calculate_total
		respond_to do |format|
			format.js
		end
	end

	private
		def item_params
			params.require(:item).permit(:paid, :product_id, :order_id, :customer_id, :price, :quantity, :name, :category_id)
		end

		def update_item_params
			params.require(:item).permit(:paid, :quantity, :price)
		end
end
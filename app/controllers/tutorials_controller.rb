class TutorialsController < ApplicationController
	def open
		@tutorial = Tutorial.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def user_tutorial
		@tutorials = Tutorial.by_position
	end

	def show
		@tutorial = Tutorial.find(params[:id])
	end

	def index
		if current_user && current_user.admin
			@tutorials = Tutorial.by_position
		else
			redirect_to root_path
		end
	end

	def create
		@tutorial = Tutorial.new(tutorial_params)
		@saved = @tutorial.save
		respond_to do |format|
			format.js
		end
	end

	def update
		@tutorial = Tutorial.find(params[:id])
		@tutorials = Tutorial.by_position
		@updated = @tutorial.update_attributes(tutorial_params)
		respond_to do |format|
			format.js
		end
	end

	def sort
		params[:tutorial].each_with_index do |id, index|
			Tutorial.where(id: id).update_all(position: index+1)
	  end

	  @tutorials = Tutorial.by_position
	  
	  respond_to do |format|
	  	format.js
	  end
	end

	private
		def tutorial_params
			params.require(:tutorial).permit(:title, :transcript, :video, :position)
		end
end

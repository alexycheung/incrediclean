class ProductsController < ApplicationController
	def load_edit
		@product = Product.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

 	def create
 		@product = Product.new(product_params)
 		@product.user_id = current_user.id
 		@saved = @product.save
 		respond_to do |format|
 			format.js
 		end
 	end

 	def update
 		@product = Product.find(params[:id])
 		@updated = @product.update_attributes(product_params)
 		respond_to do |format|
 			format.js
 		end
 	end

 	def destroy
 		@product = Product.find(params[:id])
 		@destroyed = @product.destroy
 		respond_to do |format|
 			format.js
 		end
 	end

 	private
 		def product_params
 			params.require(:product).permit(:name, :price, :img, :category_id, :cleaning_method)
 		end
end

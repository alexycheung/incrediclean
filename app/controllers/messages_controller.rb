class MessagesController < ApplicationController
	def new
		@message = Message.new
	end

	def create
		@message = Message.new(message_params)
		@message.user_id = current_user.id
		@saved = @message.save

		if @saved
			#deliver email to admin
			MessageNotifier.send_message_email(@message).deliver
		end

		respond_to do |format|
			format.js
		end
	end

	def message_params
		params.require(:message).permit(:emotion, :message_type, :content, :user_id)
	end
end
class UsersController < ApplicationController
	def set_rack
		@user        = User.find(params[:id])
		@rack_number = params[:rack]
		respond_to do |format|
			format.js
		end
	end

	def read_tutorial
		@user               = User.find(params[:id])
		@user.read_tutorial = true
		@saved              = @user.save
		respond_to do |format|
			format.js
		end
	end

	def read_intro
		@user            = User.find(params[:id])
		@user.read_intro = true
		@saved           = @user.save
		respond_to do |format|
			format.js
		end
	end

	def lost_receipt
		@user = User.find(params[:id])
		if current_user!=@user && !current_user.admin
			redirect_to root_path
		else
			@orders = @user.orders.tagless.recently.take(30)
			respond_to do |format|
				format.js
			end
		end
	end

	def urgent
		@user = User.find(params[:id])
		if current_user!=@user && !current_user.admin
			redirect_to root_path
		else
			@orders = @user.orders.urgent.recently
		end
	end

	def analytics
		@user = User.find(params[:id])
		if current_user!=@user && !current_user.admin
			redirect_to root_path
		else
			@orders     = @user.orders.recently
			@categories = Category.all

			if params[:start_date].blank? || params[:end_date].blank?
				@start_date = (DateTime.now-7.days).beginning_of_day
				@end_date   = DateTime.now.end_of_day
				@msg        = "Please enter a valid start date and end date."
			else
				split_start_date = params[:start_date].split('/')
				split_end_date   = params[:end_date].split('/')
				start_day        = split_start_date[1].to_i
				start_month      = split_start_date[0].to_i
				start_year       = split_start_date[2].to_i
				end_day          = split_end_date[1].to_i
				end_month        = split_end_date[0].to_i
				end_year         = split_end_date[2].to_i
				@start_date      = DateTime.new(start_year, start_month, start_day)+1.day
				@end_date        = DateTime.new(end_year,end_month, end_day)+1.day

				if @start_date > @end_date
					@start_date = (DateTime.now-7.days).beginning_of_day
					@end_date   = DateTime.now.end_of_day
					@msg        = "Please enter an end date that occurs after the start date."
				end
			end

			@daily_time_difference 	 = TimeDifference.between(@start_date.to_time, @end_date.to_time).in_days.ceil
			@weekly_time_difference  = TimeDifference.between(@start_date.to_time, @end_date.to_time).in_weeks.ceil
			@monthly_time_difference = TimeDifference.between(@start_date.to_time, @end_date.to_time).in_months.ceil
			@yearly_time_difference  = TimeDifference.between(@start_date.to_time, @end_date.to_time).in_years.ceil


    	@orders = @orders.where("orders.created_at >= ? AND orders.created_at <= ?", @start_date.beginning_of_day, @end_date.end_of_day)
    	
    	#calculate total revenue
			@revenue_total  = @orders.sum(:total)
			@workload_total = @orders.joins(:items).sum(:quantity)
    	
    	if !@msg && !@orders.any?
    		@msg = "No orders between your start date and end date."
    	end
    end
	end

	def confirm_password
		@user = User.find(params[:id])

		if @user.valid_password?(params[:password])
			@correct = true
		end
		respond_to do |format|
			format.js
		end
	end

	def employees
		@user = User.find(params[:id])
		if current_user!=@user && !current_user.admin
			redirect_to root_path
		else
			@employees = @user.employees.recently
		end
	end

	def tag
		@user = User.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def remember_rack
		@user = User.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def forget_rack
		@user = User.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def rack_it
		@user = User.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def index
		if current_user.admin
			@users = User.recently
		else
			redirect_to root_path
		end
	end

	def show
		unless params[:id]
			@user = User.find_by_id(current_user.id)
		else
			@user = User.find(params[:id])
		end

		@products   = @user.products
		@categories = Category.recently

		if !current_user || current_user && current_user!=@user && !current_user.admin
			redirect_to root_path
		else
			@customers = []
			
			if params[:query].present? && params[:query] != " "
				if params[:query].to_f != 0
					#if query is number, search for order

					if Order.all.search(params[:query], where: {user_id: @user.id}, fields: [:tags], misspellings: false).any?
						@order = Order.all.search(params[:query], where: {user_id: @user.id}, fields: [:tags], misspellings: false, order: {created_at: :desc}).first
					else
						@order = @user.orders.find_by_order_number(params[:query])
					end

					if @order
						@card     = @order.card
						@customer = @order.customer
						@items    = @order.items
					end
				else
					#if query is NOT a number, search for customers by name
					@customers = Customer.all.search(params[:query], where: {user_id: @user.id}, fields: [{name: :word_start}], page: params[:page], per_page: 25)
				end
			else
				@customers = @user.customers.recently_updated.page(params[:page])
			end
		end
	end

	def products
		@user = User.find(params[:id])
		if current_user!=@user && !current_user.admin
			redirect_to root_path
		else
			@categories = Category.recently
			@products   = @user.products
		end
	end
end

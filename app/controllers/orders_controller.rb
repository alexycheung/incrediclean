class OrdersController < ApplicationController
	#pay with card on pickup
	def pickup_pay_with_card
		@order = Order.find(params[:id])
		@saved = @order.pickup_pay_with_card
		respond_to do |format|
			format.js
		end
	end

	#pay with cash on pickup
	def pickup_pay_with_cash
		@order = Order.find(params[:id])
		@saved = @order.pickup_pay_with_cash
		respond_to do |format|
			format.js
		end
	end

	#rack urgent order
	def rack_urgent_order
		@order = Order.find(params[:id])

		@order.update_attributes(order_params)

		if @order.customer.text && !@order.texted
			#text customer
			QC.enqueue("Order.deliver", params[:id])
			@order.texted = true

			@updated = @order.update_attributes(order_params)			
			@user = @order.user
			@orders = @user.orders.urgent.recently
		end

		respond_to do |format|
			format.js
		end
	end

	#load form to rack urgent order
	def load_urgent_rack
		@order = Order.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def leave_note
		@order = Order.find(params[:id])
		@updated = @order.update_attributes(leave_note_params)
		respond_to do |format|
			format.js
		end
	end

	def pay_on_pickup
		@order = Order.find(params[:id])
		@customer = @order.customer
		@cards = @customer.cards.recently
		@user = @customer.user

		respond_to do |format|
			format.js
		end
	end

	def print_receipt
		@order = Order.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def delete_order
		@user = current_user

		if @user.valid_password?(params[:password])
			@order = Order.find(params[:id])
			@customer = @order.customer
			@destroyed = @order.destroy
		end

		respond_to do |format|
			format.js
		end
	end

	def confirm_delete
		@order = Order.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def add_order_number
		@order = Order.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def search_tags
		if params[:query]
			@order = current_user.orders.find_by_order_number(params[:query])
		end

		respond_to do |format|
			format.js
		end
	end

	def search_rack
		if params[:query]
			@order = current_user.orders.find_by_order_number(params[:query])
		end

		respond_to do |format|
			format.js
		end
	end

	def rack
		@order = Order.find(params[:id])

		if @order.rack!=nil && @order.customer.text && !@order.texted || @order.picked_up
			#text customer
			QC.enqueue("Order.deliver", params[:id])
			@order.texted = true

			if @order.picked_up
				@order.picked_up = false
			end
		end

		@updated = @order.update_attributes(order_params)

		respond_to do |format|
			format.js
		end
	end

	def update
		#used for tagging
		@order = Order.find(params[:id])
		@updated = @order.update_attributes(order_params)

		respond_to do |format|
			format.js
		end
	end

	#record order as picked up and record the time when it was picked up
	def pick_up
		@order = Order.find(params[:id])
		@order.picked_up = true
		@order.pickup_time = DateTime.now
		@saved = @order.save

		respond_to do |format|
			format.js
		end
	end

	def details
		@order = Order.find(params[:id])
		@card = @order.card
		@customer = @order.customer
		@items = @order.items
		respond_to do |format|
			format.js
		end
	end

	def create
		@order = Order.find(params[:id])
		@customer = @order.customer
		@order.total = @customer.calculate_total
		@saved = @order.save
		respond_to do |format|
			format.js
		end
	end

	private
		def order_params
			params.require(:order).permit(:order_number, :payment_type, :paid, :pickup_time, :tags, :rack, :card_id, :total, :customer_id, :user_id, :picked_up, :texted, :note)
		end

		def leave_note_params
			params.require(:order).permit(:note)
		end
end

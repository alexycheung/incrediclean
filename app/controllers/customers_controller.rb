class CustomersController < ApplicationController
	def pay_with_card
		@customer = Customer.find(params[:id])
		@saved = @customer.pay_with_card
		@order = @customer.orders.recently.first
		respond_to do |format|
			format.js
		end
	end

	def productless_item
		@customer = Customer.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def import
		Customer.import(params[:file], current_user)
		redirect_to root_url, notice: "Customers imported."
	end

	def pay_with_cash
		@customer = Customer.find(params[:id])
		@saved = @customer.pay_with_cash
		@order = @customer.orders.recently.first
		respond_to do |format|
			format.js
		end
	end

	def charge_later
		@customer = Customer.find(params[:id])
		@saved = @customer.charge_customer_later
		@order = @customer.orders.recently.first
		respond_to do |format|
			format.js
		end
	end

	def checkout
		@customer = Customer.find(params[:id])
		@cards = @customer.cards.recently
		@user = @customer.user

		respond_to do |format|
			format.js
		end
	end

	def details
		@customer = Customer.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	def order_history
		@customer = Customer.find(params[:id])
		@orders = @customer.orders.recently
		respond_to do |format|
			format.js
		end
	end

	def place_order
		@customer = Customer.find(params[:id])
		@items = @customer.items.not_paid.orderless
		@total = @customer.calculate_total
		respond_to do |format|
			format.js
		end
	end

	def update_phone
		@customer = Customer.find(params[:id])
		@updated = @customer.update_attributes(customer_params)
		respond_to do |format|
			format.js
		end
	end	

	def update_name
		@customer = Customer.find(params[:id])
		@updated = @customer.update_attributes(customer_params)
		respond_to do |format|
			format.js
		end
	end

	def update_starch
		@customer = Customer.find(params[:id])
		@updated = @customer.update_attributes(customer_params)
		respond_to do |format|
			format.js
		end
	end

	def update_text
		@customer = Customer.find(params[:id])
		@updated = @customer.update_attributes(customer_params)
		respond_to do |format|
			format.js
		end
	end

	def show
		@customer = Customer.find(params[:id])
		@orders = @customer.orders.not_picked_up.recently
		respond_to do |format|
			format.js
		end
	end

	def create
		@customer = Customer.new(customer_params)
		@total = 0
		@orders = @customer.orders
		if @customer.user_id.blank?
			@customer.user_id = current_user.id
		end
		@saved = @customer.save
		respond_to do |format|
			format.js
		end
	end

	private
		def customer_params
			params.require(:customer).permit(:name, :phone, :user_id, :text, :starch)
		end
end